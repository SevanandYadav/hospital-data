package com.example.hospitaldata.entity;
import javax.persistence.Column;  
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;  
import javax.persistence.Table;  
/**
 * @author 
 * Entity representing the Hospital Details
 *
 */

@Entity
@Table
public class Hospital {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	String hospitalId;
	@Column
	String hospitalName;
	@Column
	short stateCode;
	@Column
	String stateName;
	@Column
	short districtId;
	@Column
	String districtName;
	@Column
	String hospitalContNumr;
	@Column
	String hospitalAddr;
	@Column
	String hospitalType;
	
	public Hospital(String hospitalId, String hospitalName, short stateCode, String stateName, short districtId,
			String districtName, String hospitalContNumr, String hospitalAddr, String hospitalType) {
		super();
		this.hospitalId = hospitalId;
		this.hospitalName = hospitalName;
		this.stateCode = stateCode;
		this.stateName = stateName;
		this.districtId = districtId;
		this.districtName = districtName;
		this.hospitalContNumr = hospitalContNumr;
		this.hospitalAddr = hospitalAddr;
		this.hospitalType = hospitalType;
	}
	public String getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(String hospitalId) {
		this.hospitalId = hospitalId;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public short getStateCode() {
		return stateCode;
	}
	public void setStateCode(short stateCode) {
		this.stateCode = stateCode;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public short getDistrictId() {
		return districtId;
	}
	public void setDistrictId(short districtId) {
		this.districtId = districtId;
	}
	public String getDistrictName() {
		return districtName;
	}
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}
	public String getHospitalContNumr() {
		return hospitalContNumr;
	}
	public void setHospitalContNumr(String hospitalContNumr) {
		this.hospitalContNumr = hospitalContNumr;
	}
	public String getHospitalAddr() {
		return hospitalAddr;
	}
	public void setHospitalAddr(String hospitalAddr) {
		this.hospitalAddr = hospitalAddr;
	}
	public String getHospitalType() {
		return hospitalType;
	}
	public void setHospitalType(String hospitalType) {
		this.hospitalType = hospitalType;
	}

}
