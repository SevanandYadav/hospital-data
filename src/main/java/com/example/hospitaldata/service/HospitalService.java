package com.example.hospitaldata.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.hospitaldata.dao.HospitalRepository;
import com.example.hospitaldata.entity.Hospital;

/**
 * @author service for business logic
 *
 */
@Service
public class HospitalService {
	//@Autowired
	HospitalRepository hospitalRepository;

	// getting all hospital records
	public List<Hospital> getAllHospital() {
		List<Hospital> hospitals = new ArrayList<Hospital>();
		hospitalRepository.findAll().forEach(hospital -> hospitals.add(hospital));
		return hospitals;
	}

	// getting a specific record
	public Hospital getHospitalById(String id) {
		return hospitalRepository.findById(id).get();
	}

	//persist hospital details
	public void saveOrUpdate(Hospital hospital) {
		hospitalRepository.save(hospital);
	}

}
