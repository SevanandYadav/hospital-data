package com.example.hospitaldata.dao;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.hospitaldata.entity.Hospital;

@Repository
@EnableJpaRepositories(basePackages = "com.example.hospitaldata")
public interface HospitalRepository extends CrudRepository<Hospital, String> {

}
