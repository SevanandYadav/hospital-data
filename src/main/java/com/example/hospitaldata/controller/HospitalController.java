package com.example.hospitaldata.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.hospitaldata.entity.Hospital;

/**
 * @author use-case 1. Create REST API to load All Hospitals data into database
 *         from given Json. 2. Create REST API to get All Hospitals. 3. Create
 *         REST API to get Hospital by code 4. Create REST API to get Hospital
 *         by name and then by state name 5. Create REST API to get Hospitals by
 *         Hospital Type (then by name)
 *
 */
@RestController
public class HospitalController {

	public void loadHospitalDetails() {
	}

	@GetMapping("/hospitals/details")
	public Hospital[] getAllHospitalDetails() {
		return null;
	}

	@GetMapping("/hospitals/{code}")
	public Hospital getHospitalByCode(@PathVariable("code") String hospitalId) {
		//
		return null;
	}

}
