package com.example.hospitaldata.utility;

/**
 * @author description : store constants
 *
 */
public enum HospitalConstantDetails {
	HOSPITAL_PRIVATE("PRIVATE"), HOSPITAL_PUBLIC("PUBLIC");

	private final String hospitl_type;

	private HospitalConstantDetails(String hospitl_type) {
		this.hospitl_type = hospitl_type;
	}

	public String getHospitalType() {
		return this.hospitl_type;
	}
}
