DROP TABLE IF EXISTS HOSPITAL;  
CREATE TABLE HOSPITAL (  
hospitalId VARCHAR(50)  PRIMARY KEY,  
hospitalName VARCHAR(50) NOT NULL,  
stateCode INT(8) NOT NULL  ,
stateName VARCHAR(50) NOT NULL  ,
districtId INT(8) NOT NULL  ,
districtName VARCHAR(50) NOT NULL  ,
hospitalContNumr VARCHAR(15) NOT NULL  ,
hospitalAddr VARCHAR(50) NOT NULL  ,
hospitalType VARCHAR(20) NOT NULL  ,
);  
